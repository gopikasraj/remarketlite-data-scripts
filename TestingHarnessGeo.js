if (process.argv[2]) {

    var parse = require('csv-parse');
    const fs = require('fs');
    var async = require('async');
    var request = require('sync-request');
    var mysql = require('mysql');
    var mysqlCon = {
        host: 'mlslite-dev.cclzubntk2bp.us-west-2.rds.amazonaws.com',
        user: 'admin',
        password: 'Adm!nPr!v$123',
        database: 'mlslite',
        port: 3306
    };

    var APIEndPoint = 'https://dev-api.propmix.io/mlslite/val/v1/GetListingsByGeo?access_token=efb467b470e83a76f0829a142f5d0455e854677c54614fa2fa4343b0d7e67g9d&MonthsBack=12'

    var connection = mysql.createConnection(mysqlCon);
    var inputPath = process.argv[2] + '.csv';
    console.log(inputPath)
    fs.readFile(inputPath, function (err, fileData) {
        parse(fileData, { headers: true, columns: true, trim: true }, function (err, rows) {
            var max = rows.length;
            console.log('Total Zip Codes : ' + max)
            if (max > 0) {
                connection.connect(function (err) {
                    if (err) {
                        console.log('Connection Error')
                        return;
                    }
                    else {
                        connection.query('SELECT MAX(InputIdentifier) as MaxId from testing_harness_geo', function (err, maximumId) {
                            if (err) {
                                console.log('Error in finding Max Id')
                                process.exit();
                            } else {
                                var queryStart = 'INSERT INTO testing_harness_geo (InputData, InputZip, InputIdentifier, UnparsedAddress, City,StateOrProvince,PostalCode,PostalCodePlus4,ParcelNumber,PropertyType,PropertySubType,LotSizeSquareFeet,LivingArea,ArchitecturalStyle,Heating,Cooling,StoriesTotal,StoriesDescription,YearBuilt,Roof,ConstructionMaterials,BedroomsTotal,BathroomsTotalInteger,ParkingFeatures,PoolFeatures,View,PatioAndPorchFeatures,Basement,FireplacesTotal,FireplaceYN,FireplaceFeatures,InteriorFeatures,ExteriorFeatures,PublicRemarks,OtherStructures,LotFeatures,ZoningDescription,CommunityFeatures,ElementarySchoolDistrict,MiddleOrJuniorSchoolDistrict,HighSchoolDistrict,ElementarySchool,MiddleOrJuniorSchool,HighSchool,Appliances,LotSizeDimensions,Topography,WaterSource,ListingContractDate,MlsStatus,ListPrice,ClosePrice,CloseDate,StandardStatus,DistressedYN,DistressType,GarageYN,GarageSpaces,ListAgentStateLicense,ListAgentFullName,ListAgentPreferredPhone,ListAgentEmail,ListOfficeName,ListOfficePhone,ListOfficeEmail,ListingId,ListingKey,ModificationTimestamp,FIPS,PropertyRefID,PriceperSquareFeet,WaterfrontFeatures,WaterYN,DaysOnMarket,ListingType,MLSListingNumber,AgentAddress,OfficeAddress1,Address,Latitude,Longitude,BathroomsDecimal,WaterAccess,RoomsTotal,LatestListingImage,ImageCount,ImageURLs) VALUES '
                                var maxID = maximumId[0].MaxId;
                                var maxID = 100;
                                console.log('maxID : ' + maxID)
                                var APIArray = []
                                var zipArray = []
                                for (var i = 0; i < max; i++) {
                                    zipArray.push(rows[i].Zip)
                                    var eachCal = '';
                                    eachCal = rows[i].Zip && rows[i].Zip.trim() ? eachCal + '&Zip=' + rows[i].Zip.trim() : eachCal;
                                    eachCal = rows[i].MlsStatus && rows[i].MlsStatus.trim() ? eachCal + '&MlsStatus=' + rows[i].MlsStatus.trim() : eachCal
                                    // eachCal = APIEndPoint + '&Zip=' + rows[i].Zip + '&MlsStatus=' + rows[i].MlsStatus;
                                    eachCal = APIEndPoint + eachCal
                                    APIArray.push(eachCal)
                                }
                                console.log(APIArray.length)
                                var count = 0;
                                async.mapSeries(APIArray, function (item, callback) {
                                    maxID++;
                                    var res = request('GET', item);
                                    res = JSON.parse(res.getBody());
                                    console.log(res.Listings.length)
                                    var loopQry = '';
                                    var InputZip = '"' + rows[count].Zip + '"';
                                    var InputData = '"Zip : ' + rows[count]['Zip'] + ', Status : ' + rows[count]['MlsStatus'] + '"';
                                    console.log(InputData)

                                    for (var p = 0; p < res.Listings.length; p++) {
                                        var resultSet = res.Listings
                                        var InputIdentifier = maxID;
                                        var UnparsedAddress = resultSet[i].UnparsedAddress ? '"' + resultSet[i].UnparsedAddress + '"' : null;
                                        var City = resultSet[i].City ? '"' + resultSet[i].City + '"' : null;
                                        var StateOrProvince = resultSet[i].StateOrProvince ? '"' + resultSet[i].StateOrProvince + '"' : null;
                                        var PostalCode = resultSet[i].PostalCode ? '"' + resultSet[i].PostalCode + '"' : null;
                                        var PostalCodePlus4 = resultSet[i].PostalCodePlus4 ? '"' + resultSet[i].PostalCodePlus4 + '"' : null;
                                        var ParcelNumber = resultSet[i].ParcelNumber ? '"' + resultSet[i].ParcelNumber + '"' : null;
                                        var PropertyType = resultSet[i].PropertyType ? '"' + resultSet[i].PropertyType + '"' : null;
                                        var PropertySubType = resultSet[i].PropertySubType ? '"' + resultSet[i].PropertySubType + '"' : null;
                                        var LotSizeSquareFeet = resultSet[i].LotSizeSquareFeet ? '"' + resultSet[i].LotSizeSquareFeet + '"' : null;
                                        var LivingArea = resultSet[i].LivingArea ? '"' + resultSet[i].LivingArea + '"' : null;
                                        var ArchitecturalStyle = resultSet[i].ArchitecturalStyle ? '"' + resultSet[i].ArchitecturalStyle + '"' : null;
                                        var Heating = resultSet[i].Heating ? '"' + resultSet[i].Heating + '"' : null;
                                        var Cooling = resultSet[i].Cooling ? '"' + resultSet[i].Cooling + '"' : null;
                                        var StoriesTotal = resultSet[i].StoriesTotal ? '"' + resultSet[i].StoriesTotal + '"' : null;
                                        var StoriesDescription = resultSet[i].StoriesDescription ? '"' + resultSet[i].StoriesDescription + '"' : null;
                                        var YearBuilt = resultSet[i].YearBuilt ? '"' + resultSet[i].YearBuilt + '"' : null;
                                        var Roof = resultSet[i].Roof ? '"' + resultSet[i].Roof + '"' : null;
                                        var ConstructionMaterials = resultSet[i].ConstructionMaterials ? '"' + resultSet[i].ConstructionMaterials + '"' : null;
                                        var BedroomsTotal = resultSet[i].BedroomsTotal ? '"' + resultSet[i].BedroomsTotal + '"' : null;
                                        var BathroomsTotalInteger = resultSet[i].BathroomsTotalInteger ? '"' + resultSet[i].BathroomsTotalInteger + '"' : null;
                                        var ParkingFeatures = resultSet[i].ParkingFeatures ? '"' + resultSet[i].ParkingFeatures + '"' : null;
                                        var PoolFeatures = resultSet[i].PoolFeatures ? '"' + resultSet[i].PoolFeatures + '"' : null;
                                        var View = resultSet[i].View ? '"' + resultSet[i].View + '"' : null;
                                        var PatioAndPorchFeatures = resultSet[i].PatioAndPorchFeatures ? '"' + resultSet[i].PatioAndPorchFeatures + '"' : null;
                                        var Basement = resultSet[i].Basement ? '"' + resultSet[i].Basement + '"' : null;
                                        var FireplacesTotal = resultSet[i].FireplacesTotal ? '"' + resultSet[i].FireplacesTotal + '"' : null;
                                        var FireplaceYN = resultSet[i].FireplaceYN ? '"' + resultSet[i].FireplaceYN + '"' : null;
                                        var FireplaceFeatures = resultSet[i].FireplaceFeatures ? '"' + resultSet[i].FireplaceFeatures + '"' : null;
                                        var InteriorFeatures = resultSet[i].InteriorFeatures ? '"' + resultSet[i].InteriorFeatures + '"' : null;
                                        var ExteriorFeatures = resultSet[i].ExteriorFeatures ? '"' + resultSet[i].ExteriorFeatures + '"' : null;
                                        var OtherStructures = resultSet[i].OtherStructures ? '"' + resultSet[i].OtherStructures + '"' : null;
                                        var PublicRemarks = null;
                                        var LotFeatures = resultSet[i].LotFeatures ? '"' + resultSet[i].LotFeatures + '"' : null;
                                        var ZoningDescription = resultSet[i].ZoningDescription ? '"' + resultSet[i].ZoningDescription + '"' : null;
                                        var CommunityFeatures = resultSet[i].CommunityFeatures ? '"' + resultSet[i].CommunityFeatures + '"' : null;
                                        var ElementarySchoolDistrict = resultSet[i].ElementarySchoolDistrict ? '"' + resultSet[i].ElementarySchoolDistrict + '"' : null;
                                        var MiddleOrJuniorSchoolDistrict = resultSet[i].MiddleOrJuniorSchoolDistrict ? '"' + resultSet[i].MiddleOrJuniorSchoolDistrict + '"' : null;
                                        var HighSchoolDistrict = resultSet[i].HighSchoolDistrict ? '"' + resultSet[i].HighSchoolDistrict + '"' : null;
                                        var ElementarySchool = resultSet[i].ElementarySchool ? '"' + resultSet[i].ElementarySchool + '"' : null;
                                        var MiddleOrJuniorSchool = resultSet[i].MiddleOrJuniorSchool ? '"' + resultSet[i].MiddleOrJuniorSchool + '"' : null;
                                        var HighSchool = resultSet[i].HighSchool ? '"' + resultSet[i].HighSchool + '"' : null;
                                        var Appliances = resultSet[i].Appliances ? '"' + resultSet[i].Appliances + '"' : null;
                                        var LotSizeDimensions = resultSet[i].LotSizeDimensions ? '"' + resultSet[i].LotSizeDimensions + '"' : null;
                                        var Topography = resultSet[i].Topography ? '"' + resultSet[i].Topography + '"' : null;
                                        var WaterSource = resultSet[i].WaterSource ? '"' + resultSet[i].WaterSource + '"' : null;
                                        var ListingContractDate = resultSet[i].ListingContractDate ? '"' + resultSet[i].ListingContractDate + '"' : null;
                                        var MlsStatus = resultSet[i].MlsStatus ? '"' + resultSet[i].MlsStatus + '"' : null;
                                        var ListPrice = resultSet[i].ListPrice ? '"' + resultSet[i].ListPrice + '"' : null;
                                        var ClosePrice = resultSet[i].ClosePrice ? '"' + resultSet[i].ClosePrice + '"' : null;
                                        var CloseDate = resultSet[i].CloseDate ? '"' + resultSet[i].CloseDate + '"' : null;
                                        var StandardStatus = resultSet[i].StandardStatus ? '"' + resultSet[i].StandardStatus + '"' : null;
                                        var DistressedYN = resultSet[i].DistressedYN ? '"' + resultSet[i].DistressedYN + '"' : null;
                                        var DistressType = resultSet[i].DistressType ? '"' + resultSet[i].DistressType + '"' : null;
                                        var GarageYN = resultSet[i].GarageYN ? '"' + resultSet[i].GarageYN + '"' : null;
                                        var GarageSpaces = resultSet[i].GarageSpaces ? '"' + resultSet[i].GarageSpaces + '"' : null;
                                        var ListAgentStateLicense = resultSet[i].ListAgentStateLicense ? '"' + resultSet[i].ListAgentStateLicense + '"' : null;
                                        var ListAgentFullName = resultSet[i].ListAgentFullName ? '"' + resultSet[i].ListAgentFullName + '"' : null;
                                        var ListAgentPreferredPhone = resultSet[i].ListAgentPreferredPhone ? '"' + resultSet[i].ListAgentPreferredPhone + '"' : null;
                                        var ListAgentEmail = resultSet[i].ListAgentEmail ? '"' + resultSet[i].ListAgentEmail + '"' : null;
                                        var ListOfficeName = resultSet[i].ListOfficeName ? '"' + resultSet[i].ListOfficeName + '"' : null;
                                        var ListOfficePhone = resultSet[i].ListOfficePhone ? '"' + resultSet[i].ListOfficePhone + '"' : null;
                                        var ListOfficeEmail = resultSet[i].ListOfficeEmail ? '"' + resultSet[i].ListOfficeEmail + '"' : null;
                                        var ListingId = resultSet[i].ListingId ? '"' + resultSet[i].ListingId + '"' : null;
                                        var ListingKey = resultSet[i].ListingKey ? '"' + resultSet[i].ListingKey + '"' : null;
                                        var ModificationTimestamp = resultSet[i].ModificationTimestamp ? '"' + resultSet[i].ModificationTimestamp + '"' : null;
                                        var FIPS = resultSet[i].FIPS ? '"' + resultSet[i].FIPS + '"' : null;
                                        var PropertyRefID = resultSet[i].PropertyRefID ? '"' + resultSet[i].PropertyRefID + '"' : null;
                                        var PriceperSquareFeet = resultSet[i].PriceperSquareFeet ? '"' + resultSet[i].PriceperSquareFeet + '"' : null;
                                        var WaterfrontFeatures = resultSet[i].WaterfrontFeatures ? '"' + resultSet[i].WaterfrontFeatures + '"' : null;
                                        var WaterYN = resultSet[i].WaterYN ? '"' + resultSet[i].WaterYN + '"' : null;
                                        var DaysOnMarket = resultSet[i].DaysOnMarket ? '"' + resultSet[i].DaysOnMarket + '"' : null;
                                        var ListingType = resultSet[i].ListingType ? '"' + resultSet[i].ListingType + '"' : null;
                                        var MLSListingNumber = resultSet[i].MLSListingNumber ? '"' + resultSet[i].MLSListingNumber + '"' : null;
                                        var AgentAddress = resultSet[i].AgentAddress ? '"' + resultSet[i].AgentAddress + '"' : null;
                                        var OfficeAddress1 = resultSet[i].OfficeAddress1 ? '"' + resultSet[i].OfficeAddress1 + '"' : null;
                                        var Address = resultSet[i].Address ? '"' + resultSet[i].Address + '"' : null;
                                        var Latitude = resultSet[i].Latitude ? '"' + resultSet[i].Latitude + '"' : null;
                                        var Longitude = resultSet[i].Longitude ? '"' + resultSet[i].Longitude + '"' : null;
                                        var BathroomsDecimal = resultSet[i].BathroomsDecimal ? '"' + resultSet[i].BathroomsDecimal + '"' : null;
                                        var WaterAccess = resultSet[i].WaterAccess ? '"' + resultSet[i].WaterAccess + '"' : null;
                                        var RoomsTotal = resultSet[i].RoomsTotal ? '"' + resultSet[i].RoomsTotal + '"' : null;
                                        var LatestListingImage = resultSet[i].LatestListingImage ? '"' + resultSet[i].LatestListingImage + '"' : null;
                                        var ImageCount = resultSet[i].ImageCount ? '"' + resultSet[i].ImageCount + '"' : null;
                                        var ImageURLs = resultSet[i].ImageURLs ? '"' + resultSet[i].ImageURLs + '"' : null;

                                        loopQry = loopQry + '(' + InputData + ',' + InputZip + ',' + InputIdentifier + ',' + UnparsedAddress + ',' + City + ',' + StateOrProvince + ',' + PostalCode + ',' + PostalCodePlus4 + ',' + ParcelNumber + ',' + PropertyType + ',' + PropertySubType + ',' + LotSizeSquareFeet + ',' + LivingArea + ',' + ArchitecturalStyle + ',' + Heating + ',' + Cooling + ',' + StoriesTotal + ',' + StoriesDescription + ',' + YearBuilt + ',' + Roof + ',' + ConstructionMaterials + ',' + BedroomsTotal + ',' + BathroomsTotalInteger + ',' + ParkingFeatures + ',' + PoolFeatures + ',' + View + ',' + PatioAndPorchFeatures + ',' + Basement + ',' + FireplacesTotal + ',' + FireplaceYN + ',' + FireplaceFeatures + ',' + InteriorFeatures + ',' + ExteriorFeatures + ',' + OtherStructures + ',' + PublicRemarks + ',' + LotFeatures + ',' + ZoningDescription + ',' + CommunityFeatures + ',' + ElementarySchoolDistrict + ',' + MiddleOrJuniorSchoolDistrict + ',' + HighSchoolDistrict + ',' + ElementarySchool + ',' + MiddleOrJuniorSchool + ',' + HighSchool + ',' + Appliances + ',' + LotSizeDimensions + ',' + Topography + ',' + WaterSource + ',' + ListingContractDate + ',' + MlsStatus + ',' + ListPrice + ',' + ClosePrice + ',' + CloseDate + ',' + StandardStatus + ',' + DistressedYN + ',' + DistressType + ',' + GarageYN + ',' + GarageSpaces + ',' + ListAgentStateLicense + ',' + ListAgentFullName + ',' + ListAgentPreferredPhone + ',' + ListAgentEmail + ',' + ListOfficeName + ',' + ListOfficePhone + ',' + ListOfficeEmail + ',' + ListingId + ',' + ListingKey + ',' + ModificationTimestamp + ',' + FIPS + ',' + PropertyRefID + ',' + PriceperSquareFeet + ',' + WaterfrontFeatures + ',' + WaterYN + ',' + DaysOnMarket + ',' + ListingType + ',' + MLSListingNumber + ',' + AgentAddress + ',' + OfficeAddress1 + ',' + Address + ',' + Latitude + ',' + Longitude + ',' + BathroomsDecimal + ',' + WaterAccess + ',' + RoomsTotal + ',' + LatestListingImage + ',' + ImageCount + ',' + ImageURLs + '),'
                                    }
                                    loopQry = loopQry.replace(/,\s*$/, "");
                                    var insertQry = queryStart + loopQry;
                                    connection.query(insertQry, function (err, insertRes) {
                                        if (err) {
                                            console.log('query err:' + err)
                                            process.exit()
                                            callback('ERROR', null);
                                        } else {
                                            callback(null, insertRes);
                                        }
                                    })
                                    count++;
                                }, function () {
                                    console.log('Insertions Completed')
                                    process.exit();
                                });
                            }
                        })
                    }
                })
            } else {
                console.log('NO Data Found in CSV File')
            }
        })
    })
} else {
    console.log('Pass the CSV file name without extension')
}