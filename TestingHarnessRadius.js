//Trigger this script by -> node TestingHarnessRadius csvfilename
if (process.argv[2]) {
    var parse = require('csv-parse');
    const fs = require('fs');
    var async = require('async');
    var request = require('sync-request');
    var mysql = require('mysql');
    var APISet1 = 'https://api.propmix.io/mlslite/val/v1/GetListingsByRadius?PageSize=300&OrderId=testSet1&MonthsBack=12&access_token=52f048df3a3567ef4dba85129a025694b1c171abb281f5d25b3c35cc392eba74&Radius=2'
    var APISet2 = 'https://staging-api.propmix.io/mlslite/val/v1/GetListingsByRadius?PageSize=300&OrderId=testSet2&MonthsBack=12&access_token=95208b293ac014ac676ff75538011fc048fddfe065d9bf5f92eda3f12cd571b6&Radius=2'

    var mysqlCon = {
        host: 'mlslite-dev.cclzubntk2bp.us-west-2.rds.amazonaws.com',
        user: 'admin',
        password: 'Adm!nPr!v$123',
        database: 'mlslite',
        port: 3306
    };
    var connection = mysql.createConnection(mysqlCon);
    var inputPath = process.argv[2] + '.csv';
    console.log(inputPath)
    fs.readFile(inputPath, function (err, fileData) {
        parse(fileData, { headers: true, columns: true, trim: true }, function (err, rows) {
            var max = rows.length;
            console.log('Total Address : ' + max)
            if (max > 0) {
                connection.connect(function (err) {
                    if (err) {
                        console.log('Connection Error')
                        return;
                    }
                    else {
                        connection.query('SELECT MAX(AddressIdentifier) as MaxId from testing_harness_2', function (err, maximumId) {
                            if (err) {
                                console.log('Error in finding Max Id')
                                process.exit();
                            } else {
                                var maxID = maximumId[0].MaxId;
                                console.log('maxID : ' + maxID)
                                var query1 = [];
                                var addressArray = [];
                                var queryStart = ' (AddressIdentifier,inputAddress, UnparsedAddress, City,StateOrProvince,PostalCode,PostalCodePlus4,ParcelNumber,PropertyType,PropertySubType,LotSizeSquareFeet,LivingArea,ArchitecturalStyle,Heating,Cooling,StoriesTotal,StoriesDescription,YearBuilt,Roof,ConstructionMaterials,BedroomsTotal,BathroomsTotalInteger,ParkingFeatures,PoolFeatures,View,PatioAndPorchFeatures,Basement,FireplacesTotal,FireplaceYN,FireplaceFeatures,InteriorFeatures,ExteriorFeatures,PublicRemarks,OtherStructures,LotFeatures,ZoningDescription,CommunityFeatures,ElementarySchoolDistrict,MiddleOrJuniorSchoolDistrict,HighSchoolDistrict,ElementarySchool,MiddleOrJuniorSchool,HighSchool,Appliances,LotSizeDimensions,Topography,WaterSource,ListingContractDate,MlsStatus,ListPrice,ClosePrice,CloseDate,StandardStatus,DistressedYN,DistressType,GarageYN,GarageSpaces,ListAgentStateLicense,ListAgentFullName,ListAgentPreferredPhone,ListAgentEmail,ListOfficeName,ListOfficePhone,ListOfficeEmail,ListingId,ListingKey,ModificationTimestamp,FIPS,PropertyRefID,PriceperSquareFeet,WaterfrontFeatures,WaterYN,DaysOnMarket,ListingType,MLSListingNumber,AgentAddress,OfficeAddress1,Address,Latitude,Longitude,BathroomsDecimal,WaterAccess,RoomsTotal,Distance,SubjectProperty,LatestListingImage,ImageCount,ImageURLs) VALUES '
                                // var queryStart = ' (AddressIdentifier, UnparsedAddress, City) VALUES '
                                var table1 = 'INSERT INTO testing_harness_1 ' + queryStart;
                                var table2 = 'INSERT INTO testing_harness_2 ' + queryStart;

                                var rstring = '';
                                var current = 0;
                                async.mapSeries(rows, function (row, callback) {

                                    var inputAddress = '"' + row.Street + ', ' + row.City + ' , ' + row.State + ' ' + row.Zip + '"'
                                    current++;
                                    // console.log('current: ' + current)
                                    maxID++;
                                    var URL = createURL(row);
                                    var URL1 = APISet1 + URL;
                                    var URL2 = APISet2 + URL;

                                    async.parallel([
                                        function (callbackParallel) {
                                            execAndInsert(URL1, table1, function (er, resp) {
                                                if (resp) {
                                                    // console.log(resp)
                                                    if (resp == 'ERROR') { callbackParallel('Error', null) }
                                                    else { callbackParallel(null, resp) }
                                                }

                                            });

                                        },
                                        function (callbackParallel) {
                                            execAndInsert(URL2, table2, function (er, resp) {
                                                if (resp) {
                                                    // console.log(resp)
                                                    if (resp == 'ERROR') { callbackParallel('Error', null) }
                                                    else { callbackParallel(null, resp) }
                                                }

                                            });
                                        }
                                    ], function (err, results) {
                                        if (err) { console.log('async parallel throws Error'); process.exit() }
                                        console.log(current + ' ' + URL + ' Kazhnj')
                                        callback(null, row);
                                    })

                                    function execAndInsert(URL, table, clbk) {
                                        callAPI(URL, function (r) {
                                            createEachQuery(r.Listings, function qcb(qerr, Qry) {
                                                if (!qerr) {
                                                    var instertQry = table + Qry;
                                                    // console.log(instertQry)
                                                    connection.query(instertQry, function (err, insertRes) {
                                                        if (err) {
                                                            console.log('query err:' + err)
                                                            process.exit()
                                                            clbk('ERROR', null);
                                                        } else {
                                                            // console.log('query ok')
                                                            clbk(null, insertRes);
                                                        }
                                                    })
                                                } else {
                                                    console.log('Error in createEachQuery')
                                                    process.exit()
                                                }
                                            });

                                            function createEachQuery(resultSet, qcb) {
                                                try {
                                                    var loopQry = '';
                                                    console.log(resultSet.length)
                                                    for (var i = 0; i < resultSet.length; i++) {
                                                        // for (var i = 0; i < 100; i++) {
                                                        var AddressIdentifier = maxID;
                                                        var UnparsedAddress = resultSet[i].UnparsedAddress ? '"' + resultSet[i].UnparsedAddress + '"' : null;
                                                        var City = resultSet[i].City ? '"' + resultSet[i].City + '"' : null;
                                                        var StateOrProvince = resultSet[i].StateOrProvince ? '"' + resultSet[i].StateOrProvince + '"' : null;
                                                        var PostalCode = resultSet[i].PostalCode ? '"' + resultSet[i].PostalCode + '"' : null;
                                                        var PostalCodePlus4 = resultSet[i].PostalCodePlus4 ? '"' + resultSet[i].PostalCodePlus4 + '"' : null;
                                                        var ParcelNumber = resultSet[i].ParcelNumber ? '"' + resultSet[i].ParcelNumber + '"' : null;
                                                        var PropertyType = resultSet[i].PropertyType ? '"' + resultSet[i].PropertyType + '"' : null;
                                                        var PropertySubType = resultSet[i].PropertySubType ? '"' + resultSet[i].PropertySubType + '"' : null;
                                                        var LotSizeSquareFeet = resultSet[i].LotSizeSquareFeet ? '"' + resultSet[i].LotSizeSquareFeet + '"' : null;
                                                        var LivingArea = resultSet[i].LivingArea ? '"' + resultSet[i].LivingArea + '"' : null;
                                                        var ArchitecturalStyle = resultSet[i].ArchitecturalStyle ? '"' + resultSet[i].ArchitecturalStyle + '"' : null;
                                                        var Heating = resultSet[i].Heating ? '"' + resultSet[i].Heating + '"' : null;
                                                        var Cooling = resultSet[i].Cooling ? '"' + resultSet[i].Cooling + '"' : null;
                                                        var StoriesTotal = resultSet[i].StoriesTotal ? '"' + resultSet[i].StoriesTotal + '"' : null;
                                                        var StoriesDescription = resultSet[i].StoriesDescription ? '"' + resultSet[i].StoriesDescription + '"' : null;
                                                        var YearBuilt = resultSet[i].YearBuilt ? '"' + resultSet[i].YearBuilt + '"' : null;
                                                        var Roof = resultSet[i].Roof ? '"' + resultSet[i].Roof + '"' : null;
                                                        var ConstructionMaterials = resultSet[i].ConstructionMaterials ? '"' + resultSet[i].ConstructionMaterials + '"' : null;
                                                        var BedroomsTotal = resultSet[i].BedroomsTotal ? '"' + resultSet[i].BedroomsTotal + '"' : null;
                                                        var BathroomsTotalInteger = resultSet[i].BathroomsTotalInteger ? '"' + resultSet[i].BathroomsTotalInteger + '"' : null;
                                                        var ParkingFeatures = resultSet[i].ParkingFeatures ? '"' + resultSet[i].ParkingFeatures + '"' : null;
                                                        var PoolFeatures = resultSet[i].PoolFeatures ? '"' + resultSet[i].PoolFeatures + '"' : null;
                                                        var View = resultSet[i].View ? '"' + resultSet[i].View + '"' : null;
                                                        var PatioAndPorchFeatures = resultSet[i].PatioAndPorchFeatures ? '"' + resultSet[i].PatioAndPorchFeatures + '"' : null;
                                                        var Basement = resultSet[i].Basement ? '"' + resultSet[i].Basement + '"' : null;
                                                        var FireplacesTotal = resultSet[i].FireplacesTotal ? '"' + resultSet[i].FireplacesTotal + '"' : null;
                                                        var FireplaceYN = resultSet[i].FireplaceYN ? '"' + resultSet[i].FireplaceYN + '"' : null;
                                                        var FireplaceFeatures = resultSet[i].FireplaceFeatures ? '"' + resultSet[i].FireplaceFeatures + '"' : null;
                                                        var InteriorFeatures = resultSet[i].InteriorFeatures ? '"' + resultSet[i].InteriorFeatures + '"' : null;
                                                        var ExteriorFeatures = resultSet[i].ExteriorFeatures ? '"' + resultSet[i].ExteriorFeatures + '"' : null;
                                                        var OtherStructures = resultSet[i].OtherStructures ? '"' + resultSet[i].OtherStructures + '"' : null;
                                                        var PublicRemarks = null;
                                                        var LotFeatures = resultSet[i].LotFeatures ? '"' + resultSet[i].LotFeatures + '"' : null;
                                                        var ZoningDescription = resultSet[i].ZoningDescription ? '"' + resultSet[i].ZoningDescription + '"' : null;
                                                        var CommunityFeatures = resultSet[i].CommunityFeatures ? '"' + resultSet[i].CommunityFeatures + '"' : null;
                                                        var ElementarySchoolDistrict = resultSet[i].ElementarySchoolDistrict ? '"' + resultSet[i].ElementarySchoolDistrict + '"' : null;
                                                        var MiddleOrJuniorSchoolDistrict = resultSet[i].MiddleOrJuniorSchoolDistrict ? '"' + resultSet[i].MiddleOrJuniorSchoolDistrict + '"' : null;
                                                        var HighSchoolDistrict = resultSet[i].HighSchoolDistrict ? '"' + resultSet[i].HighSchoolDistrict + '"' : null;
                                                        var ElementarySchool = resultSet[i].ElementarySchool ? '"' + resultSet[i].ElementarySchool + '"' : null;
                                                        var MiddleOrJuniorSchool = resultSet[i].MiddleOrJuniorSchool ? '"' + resultSet[i].MiddleOrJuniorSchool + '"' : null;
                                                        var HighSchool = resultSet[i].HighSchool ? '"' + resultSet[i].HighSchool + '"' : null;
                                                        var Appliances = resultSet[i].Appliances ? '"' + resultSet[i].Appliances + '"' : null;
                                                        var LotSizeDimensions = resultSet[i].LotSizeDimensions ? '"' + resultSet[i].LotSizeDimensions + '"' : null;
                                                        var Topography = resultSet[i].Topography ? '"' + resultSet[i].Topography + '"' : null;
                                                        var WaterSource = resultSet[i].WaterSource ? '"' + resultSet[i].WaterSource + '"' : null;
                                                        var ListingContractDate = resultSet[i].ListingContractDate ? '"' + resultSet[i].ListingContractDate + '"' : null;
                                                        var MlsStatus = resultSet[i].MlsStatus ? '"' + resultSet[i].MlsStatus + '"' : null;
                                                        var ListPrice = resultSet[i].ListPrice ? '"' + resultSet[i].ListPrice + '"' : null;
                                                        var ClosePrice = resultSet[i].ClosePrice ? '"' + resultSet[i].ClosePrice + '"' : null;
                                                        var CloseDate = resultSet[i].CloseDate ? '"' + resultSet[i].CloseDate + '"' : null;
                                                        var StandardStatus = resultSet[i].StandardStatus ? '"' + resultSet[i].StandardStatus + '"' : null;
                                                        var DistressedYN = resultSet[i].DistressedYN ? '"' + resultSet[i].DistressedYN + '"' : null;
                                                        var DistressType = resultSet[i].DistressType ? '"' + resultSet[i].DistressType + '"' : null;
                                                        var GarageYN = resultSet[i].GarageYN ? '"' + resultSet[i].GarageYN + '"' : null;
                                                        var GarageSpaces = resultSet[i].GarageSpaces ? '"' + resultSet[i].GarageSpaces + '"' : null;
                                                        var ListAgentStateLicense = resultSet[i].ListAgentStateLicense ? '"' + resultSet[i].ListAgentStateLicense + '"' : null;
                                                        var ListAgentFullName = resultSet[i].ListAgentFullName ? '"' + resultSet[i].ListAgentFullName + '"' : null;
                                                        var ListAgentPreferredPhone = resultSet[i].ListAgentPreferredPhone ? '"' + resultSet[i].ListAgentPreferredPhone + '"' : null;
                                                        var ListAgentEmail = resultSet[i].ListAgentEmail ? '"' + resultSet[i].ListAgentEmail + '"' : null;
                                                        var ListOfficeName = resultSet[i].ListOfficeName ? '"' + resultSet[i].ListOfficeName + '"' : null;
                                                        var ListOfficePhone = resultSet[i].ListOfficePhone ? '"' + resultSet[i].ListOfficePhone + '"' : null;
                                                        var ListOfficeEmail = resultSet[i].ListOfficeEmail ? '"' + resultSet[i].ListOfficeEmail + '"' : null;
                                                        var ListingId = resultSet[i].ListingId ? '"' + resultSet[i].ListingId + '"' : null;
                                                        var ListingKey = resultSet[i].ListingKey ? '"' + resultSet[i].ListingKey + '"' : null;
                                                        var ModificationTimestamp = resultSet[i].ModificationTimestamp ? '"' + resultSet[i].ModificationTimestamp + '"' : null;
                                                        var FIPS = resultSet[i].FIPS ? '"' + resultSet[i].FIPS + '"' : null;
                                                        var PropertyRefID = resultSet[i].PropertyRefID ? '"' + resultSet[i].PropertyRefID + '"' : null;
                                                        var PriceperSquareFeet = resultSet[i].PriceperSquareFeet ? '"' + resultSet[i].PriceperSquareFeet + '"' : null;
                                                        var WaterfrontFeatures = resultSet[i].WaterfrontFeatures ? '"' + resultSet[i].WaterfrontFeatures + '"' : null;
                                                        var WaterYN = resultSet[i].WaterYN ? '"' + resultSet[i].WaterYN + '"' : null;
                                                        var DaysOnMarket = resultSet[i].DaysOnMarket ? '"' + resultSet[i].DaysOnMarket + '"' : null;
                                                        var ListingType = resultSet[i].ListingType ? '"' + resultSet[i].ListingType + '"' : null;
                                                        var MLSListingNumber = resultSet[i].MLSListingNumber ? '"' + resultSet[i].MLSListingNumber + '"' : null;
                                                        var AgentAddress = resultSet[i].AgentAddress ? '"' + resultSet[i].AgentAddress + '"' : null;
                                                        var OfficeAddress1 = resultSet[i].OfficeAddress1 ? '"' + resultSet[i].OfficeAddress1 + '"' : null;
                                                        var Address = resultSet[i].Address ? '"' + resultSet[i].Address + '"' : null;
                                                        var Latitude = resultSet[i].Latitude ? '"' + resultSet[i].Latitude + '"' : null;
                                                        var Longitude = resultSet[i].Longitude ? '"' + resultSet[i].Longitude + '"' : null;
                                                        var BathroomsDecimal = resultSet[i].BathroomsDecimal ? '"' + resultSet[i].BathroomsDecimal + '"' : null;
                                                        var WaterAccess = resultSet[i].WaterAccess ? '"' + resultSet[i].WaterAccess + '"' : null;
                                                        var RoomsTotal = resultSet[i].RoomsTotal ? '"' + resultSet[i].RoomsTotal + '"' : null;
                                                        var Distance = resultSet[i].Distance ? '"' + resultSet[i].Distance + '"' : null;
                                                        var SubjectProperty = resultSet[i].SubjectProperty ? '"' + resultSet[i].SubjectProperty + '"' : null;
                                                        var LatestListingImage = resultSet[i].LatestListingImage ? '"' + resultSet[i].LatestListingImage + '"' : null;
                                                        var ImageCount = resultSet[i].ImageCount ? '"' + resultSet[i].ImageCount + '"' : null;
                                                        var ImageURLs = resultSet[i].ImageURLs ? '"' + resultSet[i].ImageURLs + '"' : null;

                                                        loopQry = loopQry + '(' + maxID + ',' + inputAddress + ',' + UnparsedAddress + ',' + City + ',' + StateOrProvince + ',' + PostalCode + ',' + PostalCodePlus4 + ',' + ParcelNumber + ',' + PropertyType + ',' + PropertySubType + ',' + LotSizeSquareFeet + ',' + LivingArea + ',' + ArchitecturalStyle + ',' + Heating + ',' + Cooling + ',' + StoriesTotal + ',' + StoriesDescription + ',' + YearBuilt + ',' + Roof + ',' + ConstructionMaterials + ',' + BedroomsTotal + ',' + BathroomsTotalInteger + ',' + ParkingFeatures + ',' + PoolFeatures + ',' + View + ',' + PatioAndPorchFeatures + ',' + Basement + ',' + FireplacesTotal + ',' + FireplaceYN + ',' + FireplaceFeatures + ',' + InteriorFeatures + ',' + ExteriorFeatures + ',' + OtherStructures + ',' + PublicRemarks + ',' + LotFeatures + ',' + ZoningDescription + ',' + CommunityFeatures + ',' + ElementarySchoolDistrict + ',' + MiddleOrJuniorSchoolDistrict + ',' + HighSchoolDistrict + ',' + ElementarySchool + ',' + MiddleOrJuniorSchool + ',' + HighSchool + ',' + Appliances + ',' + LotSizeDimensions + ',' + Topography + ',' + WaterSource + ',' + ListingContractDate + ',' + MlsStatus + ',' + ListPrice + ',' + ClosePrice + ',' + CloseDate + ',' + StandardStatus + ',' + DistressedYN + ',' + DistressType + ',' + GarageYN + ',' + GarageSpaces + ',' + ListAgentStateLicense + ',' + ListAgentFullName + ',' + ListAgentPreferredPhone + ',' + ListAgentEmail + ',' + ListOfficeName + ',' + ListOfficePhone + ',' + ListOfficeEmail + ',' + ListingId + ',' + ListingKey + ',' + ModificationTimestamp + ',' + FIPS + ',' + PropertyRefID + ',' + PriceperSquareFeet + ',' + WaterfrontFeatures + ',' + WaterYN + ',' + DaysOnMarket + ',' + ListingType + ',' + MLSListingNumber + ',' + AgentAddress + ',' + OfficeAddress1 + ',' + Address + ',' + Latitude + ',' + Longitude + ',' + BathroomsDecimal + ',' + WaterAccess + ',' + RoomsTotal + ',' + Distance + ',' + SubjectProperty + ',' + LatestListingImage + ',' + ImageCount + ',' + ImageURLs + '),'
                                                        // loopQry = loopQry + '(' + maxID + ',' + UnparsedAddress + ',' + City + '),'
                                                    }
                                                    loopQry = loopQry.replace(/,\s*$/, "");
                                                    qcb(null, loopQry);

                                                } catch (e) {
                                                    qcb(e, null);
                                                }
                                            }
                                        });
                                    }
                                }, function (err, results) {
                                    console.log('results of asyncMap. Finished ALL ' + results);  // results : name1,name2,name3  
                                });
                            }
                        })
                    }
                })
            } else {
                console.log('NO Data Found in CSV File')
            }
        })
    })

    function createURL(adrObj) {
        var URL = '';
        URL = adrObj.Street && adrObj.Street.trim() ? URL + '&Street=' + adrObj.Street.trim() : URL;
        URL = adrObj.City && adrObj.City.trim() ? URL + '&City=' + adrObj.City.trim() : URL;
        URL = adrObj.State && adrObj.State.trim() ? URL + '&State=' + adrObj.State.trim() : URL;
        URL = adrObj.Zip && adrObj.Zip.trim() ? URL + '&Zip=' + adrObj.Zip.trim() : URL;
        URL = adrObj.Latitude && adrObj.Latitude.trim() ? URL + '&Latitude=' + adrObj.Latitude.trim() : URL;
        URL = adrObj.Longitude && adrObj.Longitude.trim() ? URL + '&Longitude=' + adrObj.Longitude.trim() : URL;
        return URL
    }

    function callAPI(callURL, cb) {

        var res = request('GET', callURL);
        res = JSON.parse(res.getBody());
        cb(res);
    }




} else {
    console.log('Pass the CSV file name without extension')
}