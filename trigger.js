"use strict";

var request = require('request');
var responseArray = [["OrderID", "Street", "State", "Zip", "TotalComparables", "TotalComparablesWithImage"]];
function callAPI(url, urlArray, index) {
    request(url, function (error, response, body) {
        if (error || !body) {
            //console.log('error:', error);
        } else if (body.indexOf('<') > -1) {
            var obj = [];
            var splitArr = addressArray[index].split('&');
            obj[1] = splitArr[1].split("=")[1];
            obj[2] = splitArr[2].split("=")[1];
            obj[3] = splitArr[3].split("=")[1];
            obj[4] = splitArr[4].split("=")[1];
            obj[5] = 'ERROR'
            obj[6] = 'ERROR'
            console.log('504 Error : '+index)
            responseArray.push(obj)
        } else {
            body = JSON.parse(body);
            var listings = body.Listings;
            var listingsWithImage = 0;
            if (listings && listings.length > 0) {
                for (var k = 0; k < listings.length; k++) {
                    if (listings[k].ImageCount > 0) { listingsWithImage++ }
                }
            }
            var obj = [];
            var splitArr = addressArray[index].split('&');
            obj[1] = splitArr[1].split("=")[1];
            obj[2] = splitArr[2].split("=")[1];
            obj[3] = splitArr[3].split("=")[1];
            obj[4] = splitArr[4].split("=")[1];;
            obj[5] = body.TotalComparables ? body.TotalComparables : 0;
            obj[6] = listingsWithImage
            responseArray.push(obj)

        }
        console.log(index);
        index++;
        if (index < count) {
            recursiveCall(urlArray, index);
        } else {
            console.log("Processing Completed")
            var respStr = responseArray.toLocaleString();

            var fs = require('fs');
            var file = fs.createWriteStream('factoryData14.txt');
            file.on('error', function (err) {
                //console.log('err')
            });
            file.write(respStr);
            file.end();
            console.log("Write ro file Completed")
        }
    });
}

var addressArray = [
    "&Orderid=2255979&Street=717 Oakland Ave&State=NY&Zip=10310",
    "&Orderid=2255981&Street=5327 Windtree Dr&State=PA&Zip=18902",
    "&Orderid=2255983&Street=841 Smith Dr&State=PA&Zip=17408",
    "&Orderid=2255985&Street=1025 Beverly Ln&State=PA&Zip=19073",
    "&Orderid=2255987&Street=434 Westervelt Ave&State=NY&Zip=10301",
    "&Orderid=2255989&Street=142 Hawk Ln&State=PA&Zip=17522",
    "&Orderid=2255991&Street=6 Minot Ave&State=MA&Zip=01720",
    "&Orderid=2256007&Street=17 Luongo Sq&State=RI&Zip=02903",
    "&Orderid=2256139&Street=288 Nelson St&State=RI&Zip=02908",
    "&Orderid=2256213&Street=10 Marial Dr&State=MA&Zip=02748",

    "&Orderid=2256217&Street=980 Providence Rd&State=MA&Zip=01588",
    "&Orderid=2256221&Street=110 Gallatin St&State=RI&Zip=02907",
    "&Orderid=2256501&Street=6 Woodland Rd&State=PA&Zip=18940",
    "&Orderid=2256509&Street=57 Damon Ave&State=MA&Zip=02176",
    "&Orderid=2256511&Street=236 Commonwealth Ave Apt 1&State=MA&Zip=02116",
    "&Orderid=2256521&Street=265 Collfield Ave&State=NY&Zip=10314",
    "&Orderid=2256553&Street=110 Russells Way&State=MA&Zip=01886",
    "&Orderid=2256603&Street=2324 Henderson St&State=PA&Zip=18017",
    "&Orderid=2256625&Street=75 Angell Ave&State=RI&Zip=02911",
    "&Orderid=2256631&Street=41 South Meadow Ln&State=RI&Zip=02806",

    "&Orderid=2256633&Street=348 Rumstick Rd&State=RI&Zip=02806",
    "&Orderid=2256741&Street=40 Fountain Rd&State=MA&Zip=02476",
    "&Orderid=2256863&Street=69 Dale St&State=MA&Zip=02119",
    "&Orderid=2257105&Street=9 3rd Ave&State=PA&Zip=19355",
    "&Orderid=2257123&Street=102 N Main St&State=MA&Zip=02702",
    "&Orderid=2257125&Street=613 School St&State=MA&Zip=02764",
    "&Orderid=2257621&Street=115 Highland St&State=MA&Zip=01520",
    "&Orderid=2257883&Street=18 Whitewood Rd&State=MA&Zip=01757",
    "&Orderid=2257911&Street=27 Forest Ave&State=NY&Zip=10301",
    "&Orderid=2257915&Street=19 Augusta Way&State=MA&Zip=02664",

    "&Orderid=2257925&Street=399 Glory Way&State=PA&Zip=17545",
    "&Orderid=2258469&Street=31 Hylan Blvd Apt 7A&State=NY&Zip=10305",
    "&Orderid=2258473&Street=33 Corning St&State=MA&Zip=01915",
    "&Orderid=2258475&Street=8 Vine St Apt 2&State=MA&Zip=01915",
    "&Orderid=2258479&Street=4 Ross Ln&State=NY&Zip=10312",
    "&Orderid=2258887&Street=98 Dam St&State=RI&Zip=02879",
    "&Orderid=2258997&Street=257 S Norwinden Dr&State=PA&Zip=19064",
    "&Orderid=2258999&Street=1639 Aidenn Lair Rd&State=PA&Zip=19025",
    "&Orderid=2259011&Street=284 Circuit St&State=MA&Zip=02339",
    "&Orderid=2259279&Street=6 New Ln Apt 4A&State=NY&Zip=10305",

    "&Orderid=2259343&Street=2944 Passmore St&State=PA&Zip=19149",
    "&Orderid=2259375&Street=24 Carolina Ct&State=NY&Zip=10314",
    "&Orderid=2259405&Street=138 Victoria Ln&State=MA&Zip=01420",
    "&Orderid=2259459&Street=212 Upper County Rd Apt 2B&State=MA&Zip=02639",
    "&Orderid=2259607&Street=367 Greenway Dr&State=PA&Zip=15235",
    "&Orderid=2259701&Street=29 Ray St&State=NY&Zip=10312",
    "&Orderid=2259703&Street=43199 W Central Ave&State=PA&Zip=16354",
    "&Orderid=2259705&Street=511 Clifton Ave&State=NY&Zip=10305",
    "&Orderid=2259707&Street=147 Brookdale St&State=MA&Zip=02364",
    "&Orderid=2259709&Street=27 Hillside Ave&State=RI&Zip=02919",
    
    "&Orderid=2259765&Street=12 Evie Dr&State=RI&Zip=02885",
    "&Orderid=2259771&Street=9 Lonsdale St&State=RI&Zip=02893",
    "&Orderid=2260421&Street=54 Florence Pl&State=NY&Zip=10309",
    "&Orderid=2260423&Street=8503 Glen Campbell Rd&State=PA&Zip=19128",
    "&Orderid=2260425&Street=8 Calvin St&State=MA&Zip=01432",
    "&Orderid=2260427&Street=133 Warren St&State=MA&Zip=02368",
    "&Orderid=2260429&Street=95 Beaver River Rd&State=RI&Zip=02892",
    "&Orderid=2260443&Street=520 Lioners Creek Rd&State=PA&Zip=17313",
    "&Orderid=2260485&Street=15 Sidney St&State=MA&Zip=02762",
    "&Orderid=2260727&Street=5331 Walton Ave&State=PA&Zip=19143",

    "&Orderid=2260733&Street=7305 N Sentinel Ln&State=PA&Zip=17403",
    "&Orderid=2260741&Street=94 Wenlock St&State=NY&Zip=10303",
    "&Orderid=2260751&Street=73 Fairlawn St&State=MA&Zip=01851",
    "&Orderid=2260803&Street=8503 Glen Campbell Rd&State=PA&Zip=19128",
    "&Orderid=2260891&Street=38 Meola Ave&State=MA&Zip=01606",
    "&Orderid=2260939&Street=1311 Narragansett St&State=PA&Zip=19138",
    "&Orderid=2260943&Street=1221 Narragansett Blvd&State=RI&Zip=02905",
    "&Orderid=2260963&Street=714 Crown St&State=PA&Zip=19067",
    "&Orderid=2261123&Street=2745 Quaker Ct&State=PA&Zip=17408",
    "&Orderid=2261357&Street=24 Guilford St&State=NY&Zip=10305",
    
    "&Orderid=2261359&Street=81 Sanford St&State=MA&Zip=02779",
    "&Orderid=2261361&Street=205 Woburn St&State=MA&Zip=02155",
    "&Orderid=2261481&Street=99 Brookwood Rd&State=NJ&Zip=07012",
    "&Orderid=2261661&Street=104 Andover Ct&State=PA&Zip=18951",
    "&Orderid=2261729&Street=4858 Sycamore Ave&State=PA&Zip=19053",
    "&Orderid=2261847&Street=555 Page St Unit 206&State=MA&Zip=02072",
    "&Orderid=2261851&Street=115 Earlington Rd&State=PA&Zip=19083",
    "&Orderid=2261983&Street=2604 S Hobson St&State=PA&Zip=19142",
    "&Orderid=2261989&Street=1139 Mason Ave&State=NY&Zip=10306",
    "&Orderid=2261993&Street=81 Emerson Gardens Rd&State=MA&Zip=02420",

    "&Orderid=2262053&Street=10 Ten Rod Rd&State=MA&Zip=01543",
    "&Orderid=2262075&Street=45 Colonial Ct&State=NY&Zip=10310",
    "&Orderid=2262269&Street=496 Huguenot Ave&State=NY&Zip=10312",
    "&Orderid=2262277&Street=57 W Plumstead Ave&State=PA&Zip=19050",
    "&Orderid=2262317&Street=624 Putnam Pike&State=RI&Zip=02828",
    "&Orderid=2262319&Street=100 Clark Ave&State=RI&Zip=02920",
    "&Orderid=2262351&Street=233 N 11th St&State=PA&Zip=18042",
    "&Orderid=2262373&Street=36 Tyler St&State=MA&Zip=02148",
    "&Orderid=2262451&Street=87 Lombard Ct&State=NY&Zip=10312",
    "&Orderid=2262481&Street=166 Hayward St&State=MA&Zip=02184",

    "&Orderid=2262609&Street=421 Richard Ave&State=NY&Zip=10309",
    "&Orderid=2262617&Street=15 Erin Ln&State=MA&Zip=01803",
    "&Orderid=2262677&Street=234 Chestnut St&State=MA&Zip=01887",
    "&Orderid=2262681&Street=50 Hill St&State=MA&Zip=01876",
    "&Orderid=2262735&Street=6 Ridgewood Rd&State=MA&Zip=01612",
    "&Orderid=2262783&Street=214 Pacific Ave&State=NY&Zip=10312",
    "&Orderid=2262941&Street=35 Old Dam Rd&State=MA&Zip=02532",
    "&Orderid=2262963&Street=35 Rhett Ave&State=NY&Zip=10308",
    "&Orderid=2262971&Street=700 Shore Dr Unit 501&State=MA&Zip=02721",
    "&Orderid=2263033&Street=113 N Ellsworth St&State=PA&Zip=18109",

    "&Orderid=2263041&Street=78 Candlewyck Dr&State=PA&Zip=17566",
    "&Orderid=2263049&Street=499 Tarrytown Ave&State=NY&Zip=10306",
    "&Orderid=2263063&Street=71 Cherry Ln&State=PA&Zip=18940",
    "&Orderid=2263081&Street=178B Kinsley St&State=NH&Zip=03060",
    "&Orderid=2263085&Street=515 S Yale St&State=PA&Zip=17403",
    "&Orderid=2263159&Street=12 Quail Ridge Dr&State=MA&Zip=01720",
    "&Orderid=2263329&Street=19 Augusta Way&State=MA&Zip=02664",
    "&Orderid=2263519&Street=196 Palmer Ave&State=NY&Zip=10302",
    "&Orderid=2263547&Street=285 S Great Rd&State=MA&Zip=01773",
    "&Orderid=2263587&Street=40 Carmel Ave&State=NY&Zip=10314",

    "&Orderid=2263657&Street=62 Hillyer Ln&State=PA&Zip=18940",
    "&Orderid=2263831&Street=1342 Swope Dr&State=PA&Zip=17007",
    "&Orderid=2263839&Street=9 Hooper Ave&State=NY&Zip=10306",
    "&Orderid=2263851&Street=68 Riverview Cir&State=MA&Zip=01778",
    "&Orderid=2263855&Street=12 Seth Davis Way&State=MA&Zip=02748",
    "&Orderid=2264017&Street=357 Fairway Ter&State=PA&Zip=19128",
    "&Orderid=2264021&Street=76 Central Ave&State=RI&Zip=02914",
    "&Orderid=2264059&Street=2 Greenbriar Dr Apt 208&State=MA&Zip=01864",
    "&Orderid=2264131&Street=117 Baboosic Lake Rd&State=NH&Zip=03054",
    "&Orderid=2264275&Street=224 Irene Ave&State=PA&Zip=17522",

    "&Orderid=2264279&Street=1401 Waterford Rd&State=PA&Zip=19067",
    "&Orderid=2264285&Street=52 Adams Ave&State=MA&Zip=02149",
    "&Orderid=2264305&Street=28 Errol St&State=RI&Zip=02888",
    "&Orderid=2264541&Street=3 Worcester Ct&State=MA&Zip=02540",
    "&Orderid=2264545&Street=1152 Pugh Rd&State=PA&Zip=19087",
    "&Orderid=2264551&Street=370 Wiese Rd&State=CT&Zip=06410",
    "&Orderid=2264751&Street=106 Alpine St&State=MA&Zip=02474",
    "&Orderid=2264829&Street=26 Tanglewood Dr&State=NY&Zip=10308",
    "&Orderid=2264917&Street=548 Drumgoole Rd W&State=NY&Zip=10312",
    "&Orderid=2264943&Street=161 Hawthorne Ct&State=PA&Zip=19610",

    "&Orderid=2264989&Street=921 Rockdale Ave&State=MA&Zip=02740",
    "&Orderid=2264993&Street=207 Elm St&State=NY&Zip=10310",
    "&Orderid=2265039&Street=31 Lillian Rd&State=MA&Zip=02420",
    "&Orderid=2265067&Street=25 King St&State=RI&Zip=02911",
    "&Orderid=2265075&Street=29 Roosevelt Ave&State=MA&Zip=02045",
    "&Orderid=2265093&Street=268 Camp Strause Rd&State=PA&Zip=17026",
    "&Orderid=2265149&Street=18 Whitewood Rd&State=MA&Zip=01757",
    "&Orderid=2265223&Street=417 Wedgewood Ln&State=PA&Zip=19063",
    "&Orderid=2265565&Street=21 Woodman Way&State=MA&Zip=01950",
    "&Orderid=2265711&Street=80 Sleepy Hollow Dr&State=RI&Zip=02864",
    "&Orderid=2265825&Street=319 Medford St&State=MA&Zip=02148",

];

var REMLendPoint = 'https://qa-api.propmix.io/mlslite/val/v1/Getlistingsbyradius?access_token=24ec91add786116af05ec0ef6d2dc077fb86e0ede5dd3341df690d6188f9918c&Radius=2&MonthsBack=18';

var RETSendPoint = 'http://staging-api.propmix.io/mlsfactory/val/v1/GetListingsByRadius?&AccessToken=92wCCH38OPZ1tP7c0ea8zr5fMkXz5t86d1h9Ru1V7D7X7nrlC07Cm914QG5kz7I9L77y0UHz368bn9gbT3if80u40H84jBlwD588WqIv4W1Gdm8RUBuVLsYhZ081KGyGUo3N0d7uUt9oFG43qtfL8beRqKsGoOgn30Rf6FimJC5wvONT798940WTR208757L8YHossF88bwBSF13aZyx11GYZ9yQ91rcBGD5LW7f1ve3CY83YNwDJXX2zK838jWE&CustomerId=PXWNCTQX&Radius=2&MonthsBack=12'

var count = addressArray.length;
console.log("count : " +count)
var urlArray = [];
for (var p = 0; p < count; p++) {
    urlArray[p] = RETSendPoint + addressArray[p];
    // console.log(urlArray[p])
}

function recursiveCall(urlArray, index) {
    var url = urlArray[index];
    callAPI(url, urlArray, index);
}
recursiveCall(urlArray, 0);